	/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};

export default class App extends Component<Props> {
	constructor(){
		super()
		this.state = {
			resultText: "",
			calculationText: ""
		}
		this.operations = ['C','+','-','*','/']
	}
	calculateResult(){
		const text = this.state.resultText 
		this.setState({
			calculationText: eval(text)
		})
	}
	buttonPressed(text){
		if(text == '='){
			return calculateResult()
		}
		this.setState({
			resultText: this.state.resultText+text
		})
	}
	operate(operation) {
		switch(operation){
			case 'D':
			let text = this.state.resultText.split('')
			text.pop()
			this.setState({
				resultText: text.join('')
			})
			break
			case '+':
			case '-':
			case '*':
			case '/':
			const lastChar = this.state.resultText.split('').pop()
			if(this.operations.indexOf(lastChar) > 0)
			if(this.state.text == "") return
			this.setState({
				resultText: this.state.resultText+operation
			})
		}
	}
	render() {
	  let rows = []
	  let nums = [[1,2,3],[4,5,6],[7,8,9],['.',0,'=']]

		for (let i = 0;i<4;i++){
			let row = []
			for(let j=0;j<3;j++){
				row.push(
				<TouchableOpacity onPress={() => this.buttonPressed(nums[i][j])} style={styles.btn}>
					<Text style= {styles.btnText}>nums[i][j]</Text>
				</TouchableOpacity>
				)
			}
			rows.push(<View style={styles.row}>{row}</View>)
		}
		
		
		let oprs = []
		for (let i=0;i<5;i++){
			oprs.push(<TouchableOpacity style={styles.btn} onPress={()=> this.operate(operations[i])}>
					<Text style= {[styles.btnText, styles.white]}>this.operations[i]</Text>
				</TouchableOpacity>)
		}
		
    return ( 
	<View style = {styles.container}>
		<View style = {styles.result}>
			<Text style = {styles.resultText}>{this.state.resultText}</Text>
		</View>
		<View style = {styles.calculation}>
			<Text style = {styles.calculationText}>{this.state.calculationText}</Text>
		</View>
		<View style = {styles.buttons}>
			<View style = {styles.numbers}>
				{rows}
			</View>
			<View style = {styles.operations }>
				{oprs}
			</View>
		</View>
	</View>
	);
  }
}

const styles = StyleSheet.create({
  container: {
	  flex:1
  },
  calculationText:{
	  fontSize: 25,
	  color: 'white'
  },
  resultText:{
	  fontSize: 30,
	  color: 'white'
  },
  btn: {
	  flex: 1,
	  alignItems: 'center',
	  alignSelf: 'stretch',
	  justifyContent: 'center'
  },
  result: {
	  flex: 2,
	  backgroundColor: 'red',
	  justifyContent: 'center',
	  alignItems: 'flex-end'
  },
  calculation: {
	  flex:1,
	  backgroundColor: 'green',
	  justifyContent: 'center',
	  alignItems: 'flex-end'
  },
  buttons: {
	  flex: 7,
	  flexDirection: 'row'
  },
  white: {
	  color: 'white'
  },
  btnText: {
	  fontSize: 30
  },
  numbers: {
	  flex: 3,
	  backgroundColor: 'yellow'
  },
  operations: {
	  flex: 1,
	  justifyContent: 'space-around',
	  alignItems: 'stretch',
	  backgroundColor: 'black'
  },
  row: {
	  flexDirection: 'row',
	  flex: 1,
	  justifyContent: 'space-around',
	  alignItems: 'center'
  }
});
